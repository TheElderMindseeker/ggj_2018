#ifndef EFTNC_SHARED_HPP
#define EFTNC_SHARED_HPP

#include "SystemManager.hpp"
#include "EntityManager.hpp"
#include "GroupManager.hpp"
#include "states/StateManager.hpp"
#include <SFML/Graphics.hpp>
#include <resource_managers/GraphicResourcesManager.hpp>
#include <resource_managers/AudioResourcesManager.hpp>

struct SharedData {
	static sf::RenderTarget *renderTo;
	static sf::Window *referenceWindow;
	static EntityManager *entityManager;
	static SystemManager *systemManager;
	static GroupManager *groupManager;
	static GraphicResourcesManager *graphicResourcesManager;
	static AudioResourcesManager *audioResourcesManager;
	static StateManager *stateManager;
};

#endif //EFTNC_SHARED_HPP
