#include "Game.hpp"
#include "Shared.hpp"

int main() {
	Game *game = new Game();
	int res = 0;
	try {
		res = game->gameLoop();
	} catch (std::exception e) {
		std::cerr << e.what() << std::endl;
	}
	delete (game);
	return res;
}