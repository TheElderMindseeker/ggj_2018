#include "systems/RadarSystem.hpp"
#include "systems/ControlsSystem.hpp"
#include "systems/GraphicsSystem.hpp"

#include "Shared.hpp"

const std::string RadarSystem::radarID = "radar";

void RadarSystem::onCreate(SystemManager *systemManager) {
	systemManager->addSystem(getID(), this);

	// Radar must update after Controls
	systemManager->addDependency(getID(), ControlsSystem::getID());
	// Ships must update after Radar
	systemManager->addDependency(ShipSystem::getID(), getID());

	SharedData::graphicResourcesManager->loadTexture("beam.png");

	createBeam();

}

void RadarSystem::createBeam() {
	auto radarEntityID =
			SharedData::entityManager->createFromSchema("radar.json");

	// get pointer to position of beam so we could easily rotate it
	beamPos = &(dynamic_cast<PositionSystem *>
	(SharedData::systemManager->getSystem(PositionSystem::getID()))
			->getComponent(radarEntityID));

	beamPos->x = (float) SharedData::referenceWindow->getSize().x / 2;
	beamPos->y = (float) SharedData::referenceWindow->getSize().y / 2;

	this->currentAngle = 0;
	beamPos->rotation = 0;

}

const std::string &RadarSystem::getID() {
	return radarID;
}

void RadarSystem::onDestroy() {}

void RadarSystem::update(sf::Time delta) {
	// Rotate radar beam
	this->currentAngle += this->rotationSpeed * delta.asMilliseconds();
	if (this->currentAngle >= 360.f) this->currentAngle -= 360.f;
	// Update beam rotation
	beamPos->rotation = this->currentAngle;

	// proceed with ships
	auto shipSystem = dynamic_cast<ShipSystem *>
	(SharedData::systemManager->getSystem(ShipSystem::getID()));

	auto pos = dynamic_cast<PositionSystem *>
	(SharedData::systemManager->getSystem(PositionSystem::getID()));

	std::vector<eftnc::entity_t> shipsEntityIDs;
	SharedData::groupManager->getEntities("ship", shipsEntityIDs);


	bool canGiveOrders = false;

	if (this->cooldown == sf::Time::Zero) {
		canGiveOrders = this->lastOrder != 0;

	} else {
		this->cooldown = std::max(this->cooldown - delta, sf::Time::Zero);

	}

	for (auto &shipID : shipsEntityIDs) {
		auto &shipPos = pos->getComponent(shipID);
		float angle = getAngle(shipPos.x, shipPos.y);

		if (isVisible(angle)) {
			shipSystem->setVisible(shipID, true);
			// std::cout << "I can see ship " << shipID << " angle: " << angle << std::endl;

			if (canGiveOrders && isTransmittable(angle)) {
				std::cout
						<< "sending order "
						<< lastOrder
						<< " to ship "
						<< shipID
						<< std::endl;

				shipSystem->sendOrder(
						shipID,
						static_cast<ShipOrder>(lastOrder)
				);
			}

		} else {
			shipSystem->setVisible(shipID, false);
		}
	}

	if (canGiveOrders) {
		std::cout
				<< "Sent order "
				<< lastOrder
				<< std::endl;

		this->cooldown = this->cooldownValue;
	}

	lastOrder = 0;

}

float RadarSystem::getAngle(int x, int y) {
	int x1 = 0;
	int y1 = -1;

	/**
	 * Vector re-calculated as if it was starting from
	 * the middle of the screen, but because it starts from top-leftmost
	 * corner of the screen (0, 0) - we "move" it.
	 * Ugh, I hope you'll understand.
	 */
	int x2 = x - SharedData::referenceWindow->getSize().x / 2;
	int y2 = y - SharedData::referenceWindow->getSize().y / 2;

	float angle = std::atan2(y2, x2) - atan2(y1, x1);
	angle = angle * 360 / (2 * M_PI);

	if (angle < 0) {
		angle = angle + 360;
	}

	return angle;
}

bool RadarSystem::isVisible(float angle) {
	float dist = std::abs(std::min(angle, this->currentAngle) - std::max(angle, this->currentAngle));

	return dist <= this->visibilityRange;

}

bool RadarSystem::isTransmittable(float angle) {
	float dist = std::abs(std::min(angle, this->currentAngle) - std::max(angle, this->currentAngle));

	return dist <= this->transmissionRange;
}
