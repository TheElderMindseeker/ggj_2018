#include <vector>
#include "InteractionSystem.hpp"
#include "CollisionSystem.hpp"
#include "PositionSystem.hpp"
#include "ProjectileSystem.hpp"

const std::string InteractionSystem::interactionID = "interaction";

void InteractionSystem::onCreate(SystemManager *systemManager) {
	systemManager->addSystem(getID(), this);
	// Interaction must execute AFTER Collision
	systemManager->addDependency(getID(), CollisionSystem::getID());
	// Interaction must execute BEFORE Position
	systemManager->addDependency(PositionSystem::getID(), getID());

	SharedData::groupManager->addGroup("ship");
	SharedData::groupManager->addGroup("friend");
	SharedData::groupManager->addGroup("enemy");
	SharedData::groupManager->addGroup("reef");
	SharedData::groupManager->addGroup("projectile");
	SharedData::groupManager->addGroup("island");

	// Load crash sound
	SharedData::audioResourcesManager->loadSound("crash.ogg");
}

const std::string &InteractionSystem::getID() {
	return interactionID;
}

void InteractionSystem::update(sf::Time delta) {
	std::vector<eftnc::entity_t> friends, enemies, reefs, projectiles, islands;
	SharedData::groupManager->getEntities("ship friend", friends);
	SharedData::groupManager->getEntities("ship enemy", enemies);
	SharedData::groupManager->getEntities("reef", reefs);
	SharedData::groupManager->getEntities("projectile", projectiles);
	SharedData::groupManager->getEntities("island", islands);

	CollisionSystem *cs = dynamic_cast<CollisionSystem *>
	(SharedData::systemManager->getSystem(CollisionSystem::getID()));
	PositionSystem *ps = dynamic_cast<PositionSystem *>
	(SharedData::systemManager->getSystem(PositionSystem::getID()));
	ProjectileSystem *proj_sys = dynamic_cast<ProjectileSystem *>
	(SharedData::systemManager->getSystem(ProjectileSystem::getID()));

	// Check reefs on collision with ships (and destroy ships if necessary)
	for (auto reef : reefs) {
		// Check friends on collision with reefs
		for (auto friendly : friends) {
			if (cs->hasIntersection(friendly, reef)) {
				SharedData::entityManager->deleteEntity(friendly);
				++num_friends_sunk;
				SharedData::audioResourcesManager->playSound("crash.ogg", true);
			}
		}
		// Check enemies on collision with reefs
		for (auto enemy : enemies) {
			if (cs->hasIntersection(enemy, reef)) {
				std::pair<float, float> opposing_force =
						cs->buildIntersectionManifold(enemy, reef);
				ps->getComponent(enemy).x += opposing_force.first;
				ps->getComponent(enemy).y += opposing_force.second;
			}
		}
		// Check collision with all projectiles (and destroy them if necessary)
		for (auto projectile : projectiles) {
			if (cs->hasIntersection(reef, projectile)) {
				SharedData::entityManager->deleteEntity(projectile);
			}
		}
	}

	// Check if the projectiles reached the ships
	for (auto projectile : projectiles) {
		// Check if projectile got out of field boundaries
		auto coords = std::make_pair(ps->getComponent(projectile).x, ps->getComponent(projectile).y);
		if (coords.first < 0 or coords.first > 700 or coords.second < 0 or coords.second > 700) {
			SharedData::entityManager->deleteEntity(projectile);
			continue;
		}

		// Destroy friendly ships which collided with projectiles
		for (auto friendly : friends) {
			if (cs->hasIntersection(projectile, friendly)
					and proj_sys->getComponent(projectile).mother_ship != friendly) {
				SharedData::entityManager->deleteEntity(friendly);
				SharedData::entityManager->deleteEntity(projectile);
				++num_friends_sunk;
				SharedData::audioResourcesManager->playSound("crash.ogg", true);
			}
		}
		// Destroy enemy ships which collided with projectiles
		for (auto enemy : enemies) {
			if (cs->hasIntersection(projectile, enemy)
				and proj_sys->getComponent(projectile).mother_ship != enemy) {
				SharedData::entityManager->deleteEntity(enemy);
				SharedData::entityManager->deleteEntity(projectile);
				++num_enemies_sunk;
				SharedData::audioResourcesManager->playSound("crash.ogg", true);
			}
		}
	}
	// Check ships collision
	for (auto friendly : friends) {
		// Check if ship got out of field boundaries
		auto coords = std::make_pair(ps->getComponent(friendly).x, ps->getComponent(friendly).y);
		if (coords.first < 0 or coords.first > 700 or coords.second < 0 or coords.second > 700) {
			SharedData::entityManager->deleteEntity(friendly);
			++num_friends_sunk;
			continue;
		}
		// Check collision between frienly ships
		for (auto other_friendly : friends) {
			if (friendly != other_friendly) {
				if (cs->hasIntersection(friendly, other_friendly)) {
					SharedData::entityManager->deleteEntity(friendly);
					SharedData::entityManager->deleteEntity(other_friendly);
					++num_friends_sunk;
					++num_friends_sunk;
					SharedData::audioResourcesManager->playSound("crash.ogg", true);
				}
			}
		}
		// Check collision with enemies
		for (auto enemy : enemies) {
			if (cs->hasIntersection(friendly, enemy)) {
				SharedData::entityManager->deleteEntity(friendly);
				SharedData::entityManager->deleteEntity(enemy);
				++num_friends_sunk;
				++num_enemies_sunk;
				SharedData::audioResourcesManager->playSound("crash.ogg", true);
			}
		}
	}
	// Check enemy ships on going out of field boundaries
	for (auto enemy : enemies) {
		// Check collision with other enemies
		for (auto other_enemy : enemies) {
			if (enemy != other_enemy) {
				if (cs->hasIntersection(enemy, other_enemy)) {
					SharedData::entityManager->deleteEntity(enemy);
					SharedData::entityManager->deleteEntity(other_enemy);
					++num_enemies_sunk;
					++num_enemies_sunk;
					SharedData::audioResourcesManager->playSound("crash.ogg", true);
				}
			}
		}
		// Check if ship got out of field boundaries
		auto coords = std::make_pair(ps->getComponent(enemy).x, ps->getComponent(enemy).y);
		if (coords.first < 0 or coords.first > 700 or coords.second < 0 or coords.second > 700) {
			SharedData::entityManager->deleteEntity(enemy);
			// Consider adding ++num_enemies_needed or --num_enemies_needed
			// to track that one ship got out of battle
			continue;
		}
	}
	// Check if the friendly or enemy ships have harboured island
	for (auto island : islands) {
		// Check collision with all friendly ships
		for (auto friendly : friends) {
			if (cs->hasIntersection(island, friendly)) {
				SharedData::entityManager->deleteEntity(friendly);
				++num_friends_harboured;
			}
		}
		// Check collision with all enemy ships
		for (auto enemy : enemies) {
			if (cs->hasIntersection(island, enemy)) {
				SharedData::entityManager->deleteEntity(enemy);
				game_lost = true;
			}
		}
		// Check collision with all projectiles (and destroy them if necessary)
		for (auto projectile : projectiles) {
			if (cs->hasIntersection(island, projectile)) {
				SharedData::entityManager->deleteEntity(projectile);
			}
		}
	}

	// Check win and lose conditions
	if (num_friends_total - num_friends_needed < num_friends_sunk)
		game_lost = true;
	if (num_friends_harboured >= num_friends_needed
		and num_enemies_sunk >= num_enemies_needed)
		game_won = true;
}

void InteractionSystem::assemble(eftnc::entity_t entity) {
	SharedData::groupManager->addEntity(entity);
	SharedData::groupManager->putIntoGroups(entity, "projectile");
}

void InteractionSystem::assemble(eftnc::entity_t entity, nlohmann::json schema) {
	SharedData::groupManager->addEntity(entity);
	SharedData::groupManager->putIntoGroups(entity, schema.at("groups").get<std::string>());
}

bool InteractionSystem::hasComponent(eftnc::entity_t entity) {
	return SharedData::groupManager->hasEntity(entity) &&
		   (SharedData::groupManager->hasGroups(entity, "ship")
			or SharedData::groupManager->hasGroups(entity, "reef")
			or SharedData::groupManager->hasGroups(entity, "projectile")
			or SharedData::groupManager->hasGroups(entity, "island"));
}

void InteractionSystem::deleteComponent(eftnc::entity_t entity) {
	SharedData::groupManager->removeFromGroups(entity, "ship friend enemy reef projectile island");
}

void InteractionSystem::onDestroy() {
	SharedData::groupManager->removeGroup("ship");
	SharedData::groupManager->removeGroup("friendly");
	SharedData::groupManager->removeGroup("enemy");
	SharedData::groupManager->removeGroup("reef");
	SharedData::groupManager->removeGroup("projectile");
	SharedData::groupManager->removeGroup("island");
}

uint8_t InteractionSystem::getFriendsHarboured() {
	return num_friends_harboured;
}

void InteractionSystem::setFriendsHarboured(uint8_t count) {
	num_friends_harboured = count;
}

uint8_t InteractionSystem::getFriendsSunk() {
	return num_friends_sunk;
}

void InteractionSystem::setFriendsSunk(uint8_t count) {
	num_friends_sunk = count;
}

uint8_t InteractionSystem::getEnemiesSunk() {
	return num_enemies_sunk;
}

void InteractionSystem::setEnemiesSunk(uint8_t count) {
	num_enemies_sunk = count;
}

uint8_t InteractionSystem::getFriendsNeeded() {
	return num_friends_needed;
}

void InteractionSystem::setFriendsNeeded(uint8_t count) {
	num_friends_needed = count;
}

uint8_t InteractionSystem::getEnemiesNeeded() {
	return num_enemies_needed;
}

void InteractionSystem::setEnemiesNeeded(uint8_t count) {
	num_enemies_needed = count;
}

uint8_t InteractionSystem::getFriendsTotal() {
	return num_friends_total;
}

void InteractionSystem::setFriendsTotal(uint8_t count) {
	num_friends_total = count;
}

uint8_t InteractionSystem::getEnemiesTotal() {
	return num_enemies_total;
}

void InteractionSystem::setEnemiesTotal(uint8_t count) {
	num_enemies_total = count;
}

bool InteractionSystem::isGameLost() {
	return game_lost;
}

void InteractionSystem::setGameLost(bool state) {
	game_lost = state;
}

bool InteractionSystem::isGameWon() {
	return game_won;
}

void InteractionSystem::setGameWon(bool state) {
	game_won = state;
}

void InteractionSystem::clearWinLoseConditions() {
	game_lost = false;
	game_won = false;
}
