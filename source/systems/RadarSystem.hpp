#ifndef EFTNC_RADAR_SYSTEM_HPP
#define EFTNC_RADAR_SYSTEM_HPP

#include "systems/AbstractSystem.hpp"
#include "systems/PositionSystem.hpp"
#include "systems/ShipSystem.hpp"

class RadarSystem : public AbstractSystem {
public:
	RadarSystem() = default;

	void onCreate(SystemManager *systemManager) override;
	void createBeam();

	void onDestroy() override;

	void update(sf::Time delta) override;

	void assemble(eftnc::entity_t) override {};

	void assemble(eftnc::entity_t, nlohmann::json) override {};

	virtual bool hasComponent(eftnc::entity_t) override { return false; };

	virtual void deleteComponent(eftnc::entity_t) override {};

	static const std::string &getID();

	/** Last order number. 0 is no order, 1 to 4 is order no. */
	int lastOrder;

protected:
	/**
	 * Calculate clock-like angle relative to center of game,
	 * where 0 degrees is 12 hours, 90 - 3 hours, and 180 - 6.
	 *
	 * @param x - x coordinate
	 * @param y - y coordinate
	 */
	float getAngle(int, int);

	/** Is given angle inside radar visibility range? */
	bool isVisible(float);

	/** Is given angle inside radar transmission range? */
	bool isTransmittable(float);

private:
	/** system ID */
	static const std::string radarID;

	/** radar beam position component reference for rotating */
	PositionComponent *beamPos;

	/** small cooldown to prevent spamming commands */
	const sf::Time cooldownValue = sf::milliseconds(500);

	/** current angle of center line of a radar */
	float currentAngle = 0;

	/** radar visibility width (radius?) */
	const float visibilityRange = 30;

	/** radar transmission width(radius?) */
	const float transmissionRange = 15;

	const float rotationSpeed = 0.072;

	/** cooldown of last order */
	sf::Time cooldown;

};

#endif // EFTNC_RADAR_SYSTEM_HPP
