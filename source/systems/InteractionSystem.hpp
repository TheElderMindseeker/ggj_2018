#ifndef EFTNC_INTERACTIONSYSTEM_HPP
#define EFTNC_INTERACTIONSYSTEM_HPP

#include "AbstractSystem.hpp"
#include <Shared.hpp>
#include <cstdint>

class InteractionSystem : public AbstractSystem {
public:
	InteractionSystem() = default;

	void onCreate(SystemManager *systemManager) override;

	static const std::string &getID();

	void update(sf::Time delta) override;

	void assemble(eftnc::entity_t entity) override;

	void assemble(eftnc::entity_t entity, nlohmann::json schema) override;

	bool hasComponent(eftnc::entity_t entity) override;

	void deleteComponent(eftnc::entity_t entity) override;

	void onDestroy() override;

	uint8_t getFriendsHarboured();

	void setFriendsHarboured(uint8_t);

	uint8_t getFriendsSunk();

	void setFriendsSunk(uint8_t);

	uint8_t getEnemiesSunk();

	void setEnemiesSunk(uint8_t count);

	uint8_t getFriendsNeeded();

	void setFriendsNeeded(uint8_t count);

	uint8_t getEnemiesNeeded();

	void setEnemiesNeeded(uint8_t count);

	uint8_t getFriendsTotal();

	void setFriendsTotal(uint8_t count);

	uint8_t getEnemiesTotal();

	void setEnemiesTotal(uint8_t count);

	bool isGameLost();

	void setGameLost(bool state);

	bool isGameWon();

	void setGameWon(bool state);

	void clearWinLoseConditions();

private:
	static const std::string interactionID;

	uint8_t num_friends_harboured;
	uint8_t num_friends_sunk;
	uint8_t num_enemies_sunk;
	uint8_t num_friends_needed;
	uint8_t num_enemies_needed;
	uint8_t num_friends_total;
	uint8_t num_enemies_total;

	bool game_lost;
	bool game_won;
};

#endif // EFTNC_INTERACTIONSYSTEM_HPP
