#include "ProjectileSystem.hpp"
#include <Shared.hpp>

const std::string ProjectileSystem::projectileID = "projectile";

void ProjectileSystem::onCreate(SystemManager *systemManager) {
	systemManager->addSystem(getID(), this);
}

const std::string &ProjectileSystem::getID() {
	return projectileID;
}

void ProjectileSystem::update(sf::Time delta) {
	// Nothing to do
}

void ProjectileSystem::assemble(eftnc::entity_t entity) {
	components.insert(std::make_pair(entity, ProjectileComponent()));
}

void ProjectileSystem::assemble(eftnc::entity_t entity, nlohmann::json schema) {
	components.insert(std::make_pair(entity, ProjectileComponent {0}));
}

bool ProjectileSystem::hasComponent(eftnc::entity_t entity) {
	return components.count(entity) > 0;
}

ProjectileComponent &ProjectileSystem::getComponent(eftnc::entity_t entity) {
	auto iter = components.find(entity);
	if (iter == components.end())
		throw eftnc::generic_exception("No such component in projectile system");
	return iter->second;
}

void ProjectileSystem::deleteComponent(eftnc::entity_t entity) {
	if (components.erase(entity) <= 0)
		throw eftnc::generic_exception("Tried to erase component not present in projectile system");
}

void ProjectileSystem::onDestroy() {
	// Nothing to do
}
