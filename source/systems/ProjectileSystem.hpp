#ifndef EFTNC_PROJECTILESYSTEM_HPP
#define EFTNC_PROJECTILESYSTEM_HPP

#include "AbstractSystem.hpp"
#include <map>
#include <components/ProjectileComponent.hpp>

class ProjectileSystem : public AbstractSystem {
public:
	void onCreate(SystemManager *systemManager) override;

	static const std::string &getID();

	void update(sf::Time delta) override;

	void assemble(eftnc::entity_t entity) override;

	void assemble(eftnc::entity_t entity, nlohmann::json schema) override;

	bool hasComponent(eftnc::entity_t entity) override;

	ProjectileComponent &getComponent(eftnc::entity_t entity);

	void deleteComponent(eftnc::entity_t entity) override;

	void onDestroy() override;

private:
	static const std::string projectileID;

	std::map<eftnc::entity_t, ProjectileComponent> components;
};

#endif // EFTNC_PROJECTILESYSTEM_HPP
