#include "ShipSystem.hpp"
#include "Shared.hpp"
#include "PositionSystem.hpp"
#include "GraphicsSystem.hpp"
#include "VelocitySystem.hpp"
#include "ProjectileSystem.hpp"

const std::string ShipSystem::shipID = "ship";

void ShipSystem::onCreate(SystemManager *systemManager) {
	systemManager->addSystem(getID(), this);
	SharedData::audioResourcesManager->loadSound("morse_succ.ogg");
	SharedData::audioResourcesManager->loadSound("morse_miss.ogg");
	SharedData::audioResourcesManager->loadSound("cannon.ogg");

}

const std::string &ShipSystem::getID() {
	return shipID;
}

void ShipSystem::update(sf::Time delta) {
	for (ShipComponent &ship:ships) {
		if (ship.order_cooldown > 0)
			if (delta.asMilliseconds() > ship.order_cooldown)
				ship.order_cooldown = 0;
			else
				ship.order_cooldown -= delta.asMilliseconds();
	}
}

void ShipSystem::assemble(eftnc::entity_t entity) {
	throw new eftnc::generic_exception("ShipSystem: can't create an empty ship!");
}

void ShipSystem::assemble(eftnc::entity_t entity, nlohmann::json schema) {
	ShipComponent added;
	// Has_gun is needed only for graphics and CAN, theoretically, be derived from the current controls
	// But this approach might be simpler.
	// Anyway,
	// TODO : Read this and only affect the graphics.
	bool has_gun = schema.value("has_gun", false);

	added.entity = entity;
	added.color_scheme = schema["color_scheme"].get<uint8_t>();
	// I'm SURE this won't happen in this time, but it makes sense to check just to be sure.
	if (added.color_scheme > 6 || added.color_scheme < 0)
		throw new eftnc::generic_exception("ShipSystem:Tried to create a ship with an invalid color!");

	// Default values
	// Initial visibility is set to false, just in case.
	added.is_visible = true;
	added.order_cooldown = 0;

	GraphicsSystem *graph = dynamic_cast<GraphicsSystem *>(
			SharedData::systemManager->getSystem(GraphicsSystem::getID()));

	PositionSystem *pos = dynamic_cast<PositionSystem *>(
			SharedData::systemManager->getSystem(PositionSystem::getID()));

	if (added.color_scheme < 3) {
		added.shadow_entity = SharedData::entityManager->createFromSchema("ship_shadow_enemy.json");
	} else {
		added.shadow_entity = SharedData::entityManager->createFromSchema("ship_shadow_friend.json");
	}
	graph->toggleFlags(added.shadow_entity, GRAPHICS_VISIBLE);
	PositionComponent shadow_pos = pos->getComponent(added.shadow_entity);
	PositionComponent normal_pos = pos->getComponent(added.entity);
	shadow_pos = normal_pos;

	ships.push_back(added);
}

bool ShipSystem::hasComponent(eftnc::entity_t entity) {
	for (ShipComponent component:ships) {
		if (component.entity == entity)
			return true;
	}
	return false;
}

void ShipSystem::deleteComponent(eftnc::entity_t entity) {
	for (int c = 0; c < ships.size(); ++c) {
		if (ships[c].entity == entity) {
			SharedData::entityManager->deleteEntity(ships[c].shadow_entity);
			ships[c] = ships[ships.size() - 1];
			ships.pop_back();
		}
	}
}

void ShipSystem::onDestroy() {
	// I THINK this is correct.
	ships.clear();
}

void ShipSystem::setVisible(eftnc::entity_t entity, bool state) {
	// TODO : REFACTOR THIS SHIT, REALLY.
	PositionSystem *sys = NULL;
	GraphicsSystem *graph = NULL;
	if (SharedData::systemManager->getSystem(PositionSystem::getID())) {
		sys = dynamic_cast<PositionSystem *>(
				SharedData::systemManager->getSystem(PositionSystem::getID()));
		graph = dynamic_cast<GraphicsSystem *>(SharedData::systemManager->getSystem(GraphicsSystem::getID()));
	}
	if (state) {
		for (int c = 0; c < ships.size(); ++c) {
			if (ships[c].entity == entity) {
				if (ships[c].is_visible)
					return;
				graph->toggleFlags(ships[c].shadow_entity, GRAPHICS_VISIBLE);
				graph->toggleFlags(entity, GRAPHICS_VISIBLE);
				ships[c].is_visible = true;
			}
		}
	} else {
		for (int c = 0; c < ships.size(); ++c) {
			if (ships[c].entity == entity) {
				if (!ships[c].is_visible)
					return;
				graph->toggleFlags(ships[c].shadow_entity, GRAPHICS_VISIBLE);
				graph->toggleFlags(entity, GRAPHICS_VISIBLE);

				PositionComponent &orig = sys->getComponent(entity);
				PositionComponent &shadow = sys->getComponent(ships[c].shadow_entity);

				shadow.x = orig.x;
				shadow.y = orig.y;
				shadow.rotation = orig.rotation;
				ships[c].is_visible = false;
			}
		}
	}
}

void ShipSystem::setControl(uint8_t color_scheme, ShipOrder control_scheme[4]) {
	if (color_scheme > 6 || color_scheme < 0)
		throw new eftnc::generic_exception("ShipSystem:Tried to set controls to an invalid color!");

	for (int c = 0; c < 4; ++c) {
		control_schemes[color_scheme][c] = control_scheme[c];
	}
}

void ShipSystem::sendOrder(eftnc::entity_t entity, ShipOrder order) {
	PositionSystem *pos_sys = NULL;
	VelocitySystem *vel_sys = NULL;
	// Cached outside the loop
	if (SharedData::systemManager->getSystem(PositionSystem::getID())) {
		pos_sys = dynamic_cast<PositionSystem *>(
				SharedData::systemManager->getSystem(PositionSystem::getID()));
	}
	if (SharedData::systemManager->getSystem(VelocitySystem::getID())) {
		vel_sys = dynamic_cast<VelocitySystem *>(
				SharedData::systemManager->getSystem(VelocitySystem::getID()));
	}
	for (int c = 0; c < ships.size(); ++c) {
		if (ships[c].entity == entity) {
			order = control_schemes[ships[c].color_scheme][order - 1];
			switch (order) {
				case SHIP_NONE:
					// Show confusion
					SharedData::audioResourcesManager->playSound("morse_miss.ogg");
					break;
				case SHIP_RIGHT: {
					SharedData::audioResourcesManager->playSound("morse_succ.ogg");
					VelocityComponent &vel = vel_sys->getComponent(entity);
					VelocityComponent new_vel;
					PositionComponent &pos = pos_sys->getComponent(entity);
					new_vel.x_vel = vel.x_vel * COS_45 - vel.y_vel * SIN_45;
					new_vel.y_vel = vel.x_vel * SIN_45 + vel.y_vel * COS_45;
					vel = new_vel;
					pos.rotation += 45;
					while (pos.rotation >= 360)
						pos.rotation = pos.rotation - 360;
					break;
				}
				case SHIP_LEFT: {
					SharedData::audioResourcesManager->playSound("morse_succ.ogg");
					VelocityComponent &vel = vel_sys->getComponent(entity);
					VelocityComponent new_vel;
					PositionComponent &pos = pos_sys->getComponent(entity);
					new_vel.x_vel = vel.x_vel * COS_45 + vel.y_vel * SIN_45;
					new_vel.y_vel = -vel.x_vel * SIN_45 + vel.y_vel * COS_45;
					vel = new_vel;
					pos.rotation -= 45;
					while (pos.rotation < 0)
						pos.rotation = 360 + pos.rotation;
					break;
				}
				case SHIP_SHOOT: {
					// Play cannon shot sound
					SharedData::audioResourcesManager->playSound("cannon.ogg");

					ProjectileSystem *proj_sys = dynamic_cast<ProjectileSystem *>
					(SharedData::systemManager->getSystem(ProjectileSystem::getID()));

					PositionComponent ship_pos = pos_sys->getComponent(entity);

					// Create standard projectile
					eftnc::entity_t new_projectile = SharedData::entityManager->createFromSchema("projectile.json");
					pos_sys->assemble(new_projectile);
					vel_sys->assemble(new_projectile);
					// Alter position
					pos_sys->getComponent(new_projectile).x = pos_sys->getComponent(entity).x;
					pos_sys->getComponent(new_projectile).y = pos_sys->getComponent(entity).y;
					// Alter velocity
					vel_sys->getComponent(new_projectile).x_vel = (float) std::sin(ship_pos.rotation * M_PI / 180) * 50;
					vel_sys->getComponent(new_projectile).y_vel =
							(float) -std::cos(ship_pos.rotation * M_PI / 180) * 50;
					// Set mothership id to current shooting ship id
					proj_sys->getComponent(new_projectile).mother_ship = entity;
					break;
				}
			}
		}
	}
}
