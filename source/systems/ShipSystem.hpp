//
// Created by Alex on 27.01.2018.
//

#ifndef GGJ_2018_SHIPSYSTEM_H
#define GGJ_2018_SHIPSYSTEM_H

#include "AbstractSystem.hpp"

#include <cmath>

enum ShipOrder {
	SHIP_NONE = 1,
	SHIP_RIGHT,
	SHIP_LEFT,
	SHIP_SHOOT
};

const uint32_t DEFAULT_ORDER_COOLDOWN = 1000;
const float COS_45 = std::cos(45 * M_PI / 180.f);
const float SIN_45 = std::sin(45 * M_PI / 180.f);

struct ShipComponent {
	eftnc::entity_t entity;

	uint8_t color_scheme;

	// TODO : Purely for graphics, don't need to store it
	bool has_gun;

	uint32_t order_cooldown;

	bool is_visible;

	/*
	 * Now, the trick here is:
	 * When the normal entity is visible, the shadow entity is invisible.
	 * When the normal entity becomes invisible, the shadow entity is teleported in its place
	 */
	eftnc::entity_t shadow_entity;
};

class ShipSystem : public AbstractSystem {
public:
	// Inherited from AbstractSystem
	void onCreate(SystemManager *systemManager) override;

	static const std::string &getID();

	void update(sf::Time delta) override;

	void assemble(eftnc::entity_t entity) override;

	/* Assemble fields:
	 * color_scheme - int, [0 to 5] - 0-2 is enemy, 3-5 is friendly
	 * has_gun - bool
	 *
	 * Order mappings are not included since they are more of a per-level thing than a per-instance thing
	 */
	void assemble(eftnc::entity_t entity, nlohmann::json schema) override;

	bool hasComponent(eftnc::entity_t entity) override;

	void deleteComponent(eftnc::entity_t entity) override;

	void onDestroy() override;

	// Personal functions
	void setControl(uint8_t color_scheme, ShipOrder control_scheme[4]);

	void sendOrder(eftnc::entity_t, ShipOrder order);

	void setVisible(eftnc::entity_t entity, bool state);

private:
	std::vector<ShipComponent> ships;
	ShipOrder control_schemes[6][4];

	const static std::string shipID;
};


#endif //GGJ_2018_SHIPSYSTEM_H
