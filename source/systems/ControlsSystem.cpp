#include "components/VelocityComponent.hpp"

#include "components/inputs/KeyboardKeyInput.hpp"
#include "components/inputs/JoystickInput.hpp"
#include "components/inputs/MouseInput.hpp"

#include "ControlsSystem.hpp"
#include "VelocitySystem.hpp"
#include "RadarSystem.hpp"
#include "InteractionSystem.hpp"
#include "ShipSystem.hpp"

#include "Shared.hpp"

const std::string ControlsSystem::controlsID = "controls";

const std::string &ControlsSystem::getID() {
	return controlsID;
}

void ControlsSystem::onCreate(SystemManager *systemManager) {
	systemManager->addSystem(getID(), this);
}

void ControlsSystem::onDestroy() {}

void ControlsSystem::assemble(eftnc::entity_t entity) {
	ControlsComponent controls{entity, {
			{eftnc::input::Action::UP, std::make_shared<KeyboardKeyInput>(sf::Keyboard::Key::W)},
			{eftnc::input::Action::DOWN, std::make_shared<KeyboardKeyInput>(sf::Keyboard::Key::S)},
			{eftnc::input::Action::LEFT, std::make_shared<KeyboardKeyInput>(sf::Keyboard::Key::A)},
			{eftnc::input::Action::RIGHT, std::make_shared<KeyboardKeyInput>(sf::Keyboard::Key::D)}
	}};

	components.push_back(controls);

}

static const std::map<std::string, eftnc::input::Action> actionStrings{
		{"UP",        eftnc::input::Action::UP},
		{"DOWN",      eftnc::input::Action::DOWN},
		{"LEFT",      eftnc::input::Action::LEFT},
		{"RIGHT",     eftnc::input::Action::RIGHT},
		{"FIRE1",     eftnc::input::Action::FIRE1},
		{"AIM",       eftnc::input::Action::AIM},
		{"AIM_X_POS", eftnc::input::Action::AIM_X_POS},
		{"AIM_X_NEG", eftnc::input::Action::AIM_X_NEG},
		{"AIM_Y_POS", eftnc::input::Action::AIM_Y_POS},
		{"AIM_Y_NEG", eftnc::input::Action::AIM_Y_NEG},
		{"ORDER0",    eftnc::input::Action::ORDER0},
		{"ORDER1",    eftnc::input::Action::ORDER1},
		{"ORDER2",    eftnc::input::Action::ORDER2},
		{"ORDER3",    eftnc::input::Action::ORDER3}
};

void ControlsSystem::assemble(eftnc::entity_t entity, nlohmann::json schema) {
	ControlsComponent tComponent;

	tComponent.entity_id = entity;

	auto con_schema = schema["control_schema"];

	for (auto &con : con_schema) {
		std::string actionString = con.value("action", "NULL");

		int value = con.value("value", -1);

		if (actionString == "NULL" || value == -1)
			continue;

		eftnc::input::Action action;

		try {
			action = actionStrings.at(actionString);
		} catch (const std::out_of_range &e) {
			std::cerr << "Invalid Action: " << e.what() << std::endl;
			continue;
		}

		auto t = con.value("type", "NULL");
		if (t == "NULL") {
			continue;

		} else if (t == "joyA") {
			uint8_t joyNo = con.value("joyNo", 0);
			auto axis = static_cast<sf::Joystick::Axis>(value);
			bool isNeg = con.value("isNeg", false);

			tComponent.controls[action] = std::make_shared<JoystickAxisInput>(joyNo, axis, isNeg);

		} else if (t == "joyB") {
			int joyNo = con.value("joyNo", 0);

			tComponent.controls[action] = std::make_shared<JoystickButtonInput>(joyNo, value);

		} else if (t == "keyboard") {
			auto button = static_cast<sf::Keyboard::Key>(value);

			tComponent.controls[action] = std::make_shared<KeyboardKeyInput>(button);

		} else if (t == "mouseB") {
			auto button = static_cast<sf::Mouse::Button>(value);

			tComponent.controls[action] = std::make_shared<MouseButtonInput>(button);

		} else if (t == "mouseW") {
			auto wheel = static_cast<sf::Mouse::Wheel>(value);
			bool isNeg = con.value("isNeg", false);

			tComponent.controls[action] = std::make_shared<MouseWheelInput>(wheel, isNeg);

		} else if (t == "mouseP") {
			tComponent.controls[action] = std::make_shared<MousePointerInput>();

		}
	}

	this->components.push_back(tComponent);
}

void ControlsSystem::newComponent(ControlsComponent &component) {
	components.push_back(component);
}

static const int speed = 50;

void ControlsSystem::update(sf::Time delta) {
	// TODO: Find more effective way to get systems without
	// rewriting entire ControlSystem, lol. Callbacks? :3
	auto radar = dynamic_cast<RadarSystem *>
	(SharedData::systemManager->getSystem(RadarSystem::getID()));

	// radar->lastOrder = 0;

	for (auto &component : components) {
		for (auto &control : component.controls) {

			control.second->update();
			auto action = control.first;

			switch (action) {
				case eftnc::input::FIRE1 : {
					bool isPressed;

					handleLinearControl(control.second, isPressed);
					if (isPressed) {
						dynamic_cast<InteractionSystem *>
						(SharedData::systemManager->getSystem(InteractionSystem::getID()))
								->setGameWon(true);
					}

					break;
				}
				case eftnc::input::ORDER0 : {
					handleLinearControl(control.second, radar->lastOrder, ShipOrder::SHIP_NONE);

					break;
				}
				case eftnc::input::ORDER1 : {
					handleLinearControl(control.second, radar->lastOrder, ShipOrder::SHIP_RIGHT);

					break;
				}
				case eftnc::input::ORDER2 : {
					handleLinearControl(control.second, radar->lastOrder, ShipOrder::SHIP_LEFT);

					break;
				}
				case eftnc::input::ORDER3 : {
					handleLinearControl(control.second, radar->lastOrder, ShipOrder::SHIP_SHOOT);

					break;
				}

				default:
					break;
			}
		}
	}
}

// We don't need to delete control components quite often, this should be enough
void ControlsSystem::deleteComponent(eftnc::entity_t entity) {
	// std::vector<struct ControlsComponent>::const_iterator iter;
	for (auto iter = components.begin(); iter != components.end(); iter++) {
		if (iter->entity_id == entity) {
			components.erase(iter);
			return;
		}
	}
}

bool ControlsSystem::hasComponent(eftnc::entity_t entity_id) {
	for (auto &component : components) {
		if (component.entity_id == entity_id)
			return true;
	}
	return false;
}

// TODO: resolve not found?
ControlsComponent &ControlsSystem::getComponent(eftnc::entity_t entity_id) {
	auto it = components.begin();
	for (; it != components.end() && it->entity_id != entity_id; it++);
	return *it;
}

template<typename T>
void ControlsSystem::handleLinearControl(std::shared_ptr<Input> input, T &dest, float multiplier) {
	bool keyState;
	float axisValue;
	float wheelDelta;

	void *rawState = input->getState();

	switch (input->getType()) {
		case eftnc::input::InputType::KeyboardKey:
		case eftnc::input::InputType::JoystickButton:
		case eftnc::input::InputType::MouseButton:
			// Not prettiest way, but hey, it will work with anything unlike union
			keyState = *(static_cast<bool *>(rawState));

			if (keyState)
				dest = multiplier;

			break;

		case eftnc::input::InputType::JoystickAxis:
			axisValue = *(static_cast<float *>(rawState));

			if (axisValue > 0)
				dest = (axisValue / 100) * multiplier;

			break;

		case eftnc::input::InputType::MouseWheel:
			wheelDelta = *(static_cast<float *>(rawState));

			if (wheelDelta != 0)
				dest = multiplier;

			break;

		case eftnc::input::InputType::MousePos:
			// Not applicable in linear controls
			break;
	}

}
