#ifndef EFTNC_SPRITE_RESOURCES_MANAGER_HPP
#define EFTNC_SPRITE_RESOURCES_MANAGER_HPP

#include "json/json.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System/Time.hpp>

#include <unordered_map>

/**
 * Animation Resource structure.
 */
struct AnimationResource {
	std::string name;        // probably useless
	sf::Time frameTime;    // how long one frame should play
	uint16_t nFrames;    // how many frames in animation
	sf::IntRect frame;        // first frame of on spritesheet; next frames are same rectangles on the right
	bool loop;        // should animation loop or stop on last frame
};

/**
 * Graphic Resources Manager is responsible for loading
 * Animations in json file format, parsing them into
 * smaller structure and
 */
class GraphicResourcesManager {
public:
	int loadJSON(std::string);

	int loadTexture(const std::string &);

	int loadTexture(const std::string &, sf::Texture);

	// TODO : Hopefully temporary
	int addAnimation(AnimationResource);

	/**
	 * Load required textures and turn JSON into smaller Animation structure
	 * @return
	 */
	int parseAnimations();

	/**
	 * Get index of animation in animations vector for faster access
	 * @return index of animation
	 */
	uint64_t getAnimationIndex(std::string);

	uint64_t getTextureIndex(const std::string &);

	AnimationResource &getAnimation(uint64_t);

	sf::Texture &getTexture(uint64_t);

private:
	std::string tex_path = "resources/sprites/";
	std::string ani_path = "resources/animations/";

	std::unordered_map<std::string, nlohmann::json> jsons;

	std::unordered_map<std::string, uint64_t> animation_indexes;
	std::unordered_map<std::string, uint64_t> textures_indexes;

	std::vector<AnimationResource> animations;
	std::vector<sf::Texture> textures;

};


#endif //EFTNC_SPRITE_RESOURCES_MANAGER_HPP
