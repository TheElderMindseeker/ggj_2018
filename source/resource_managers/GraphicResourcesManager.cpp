#include "GraphicResourcesManager.hpp"

#include <fstream>

int GraphicResourcesManager::loadJSON(std::string fileName) {
	// Load file
	std::string full_path = ani_path + fileName;
	std::ifstream infile(full_path);
	if (!infile.good()) {
		return -1;
	}

	// Deserialize JSON object
	infile >> this->jsons[fileName];

	return 0;
}

int GraphicResourcesManager::loadTexture(const std::string &fileName) {
	sf::Texture texture;
	if (!texture.loadFromFile(tex_path + fileName)) {
		return -1;
	}
	return loadTexture(fileName, texture);
}

int GraphicResourcesManager::loadTexture(const std::string &fileName, sf::Texture texture) {
	if (textures_indexes.find(fileName) == textures_indexes.end()) {
		this->textures.push_back(texture);
		this->textures_indexes[fileName] = textures.size() - 1;
	}

	return 0;
}

int GraphicResourcesManager::addAnimation(AnimationResource animation) {
	if (animation_indexes.find(animation.name) == animation_indexes.end()) {
		this->animations.push_back(animation);
		this->animation_indexes[animation.name] = animations.size() - 1;
	}
	return 0;
}

int GraphicResourcesManager::parseAnimations() {
	for (auto pair : jsons) {
		auto json = pair.second;

		if (this->loadTexture(json["texture"]) == -1) {
			std::cerr << "unable to load texture" << json["texture"] << std::endl;
			return -1;
		}

		for (auto anim : json["animations"]) {
			AnimationResource t = {
					anim["name"].get<std::string>(),
					sf::milliseconds(anim["frameTime"].get<int>()),
					anim["nFrames"].get<uint16_t>(),
					sf::IntRect(anim["frame"][0].get<int>(),
								anim["frame"][1].get<int>(),
								anim["frame"][2].get<int>(),
								anim["frame"][3].get<int>()
					),
					anim.value("loop", true)
			};

			this->animations.push_back(t);
			std::string tex_name = json["texture"].get<std::string>() + "_" + anim["name"].get<std::string>();
			this->animation_indexes[tex_name] = animations.size() - 1;

		}
	}

	jsons.clear();

	return 0;
}

uint64_t GraphicResourcesManager::getAnimationIndex(std::string animationName) {
	return animation_indexes.at(animationName);
}

uint64_t GraphicResourcesManager::getTextureIndex(const std::string &fileName) {
	return textures_indexes.at(fileName);
}

AnimationResource &GraphicResourcesManager::getAnimation(uint64_t index) {
	return animations[index];
}

sf::Texture &GraphicResourcesManager::getTexture(uint64_t index) {
	return textures[index];
}

