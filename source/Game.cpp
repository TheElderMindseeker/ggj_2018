#include <systems/PositionSystem.hpp>
#include <systems/VelocitySystem.hpp>
#include <systems/GraphicsSystem.hpp>
#include <systems/CollisionSystem.hpp>
#include <systems/ControlsSystem.hpp>
#include <systems/InteractionSystem.hpp>
#include <systems/ShipSystem.hpp>
#include <systems/RadarSystem.hpp>
#include <systems/ProjectileSystem.hpp>
#include <Game.hpp>
#include <Shared.hpp>

#include <states/EmptyState.hpp>
#include <states/ReefState.hpp>
#include "states/MessageState.hpp"

Game::Game() : entityManager(&systemManager), groupManager(200, 10) {
	window.create(sf::VideoMode(700, 700), "brODDcast", sf::Style::Close);

	window.setFramerateLimit(60);

	SharedData::renderTo = &window;
	SharedData::referenceWindow = &window;
	SharedData::entityManager = &entityManager;
	SharedData::systemManager = &systemManager;
	SharedData::graphicResourcesManager = &m_graphicsMan;
	SharedData::groupManager = &groupManager;
	SharedData::audioResourcesManager = &m_audioMan;

	m_audioMan.loadMusic("danse_macabre.ogg");
	m_audioMan.playMusic();

	auto pos = new PositionSystem();
	auto vel = new VelocitySystem();
	auto graphics = new GraphicsSystem(&m_graphicsMan);
	auto collision = new CollisionSystem();
	auto controls = new ControlsSystem();
	auto interaction = new InteractionSystem();
	auto ship = new ShipSystem();
	auto radar = new RadarSystem();
	auto proj = new ProjectileSystem();

	pos->onCreate(&systemManager);
	vel->onCreate(&systemManager);
	graphics->onCreate(&systemManager);
	collision->onCreate(&systemManager);
	controls->onCreate(&systemManager);
	interaction->onCreate(&systemManager);
	ship->onCreate(&systemManager);
	radar->onCreate(&systemManager);
	proj->onCreate(&systemManager);
}

int Game::gameLoop() {
	clock.restart();

	StateManager sm;
	// sm.pushState(std::make_unique<EmptyState>());

	SharedData::stateManager = &sm;

	// NOTE : Another hack. Level 0 means initial state
	sm.pushState(std::make_unique<ReefState>(0));
	// NOTE: Kinda a hack
	sm.pushState(std::make_unique<MessageState>(7, "Push Z, X, C, V to \nsend some signals to the ships."));
	sm.pushState(std::make_unique<MessageState>(5,
				"You are stuck on this island.\n"
				"Try using the transmitter.\n"));

	while (this->window.isOpen()) {
		sf::Event event = sf::Event();

		// Mouse Wheel events
		extern float HWheelDelta;
		extern float VWheelDelta;

		HWheelDelta = 0;
		VWheelDelta = 0;

		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				this->window.close();
				break;
			}

			if (event.type == sf::Event::MouseWheelScrolled) {
				if (event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel)
					VWheelDelta = event.mouseWheelScroll.delta;

				else if (event.mouseWheelScroll.wheel == sf::Mouse::HorizontalWheel)
					HWheelDelta = event.mouseWheelScroll.delta;
			}

		}

		window.clear(sf::Color::Black);
		sf::Time timeElapsed = clock.restart();

		sm.update(timeElapsed);

		window.display();
		entityManager.cleanup();
	}

	return 0;
}
