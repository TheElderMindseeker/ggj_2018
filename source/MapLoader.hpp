//
// Created by Alex on 27.01.2018.
//

#ifndef GGJ_2018_MAPLOADER_HPP
#define GGJ_2018_MAPLOADER_HPP

#include "Types.hpp"

// This class only LOADS the map, everything else is up to the state
class MapLoader {
public:
	static void load_map(const std::string &filename);

	static bool map_exists(const std::string &filename);
};


#endif //GGJ_2018_MAPLOADER_HPP
