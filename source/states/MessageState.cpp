//
// Created by Alex on 27.01.2018.
//

#include "MessageState.hpp"

#include "Shared.hpp"

void MessageState::atStart() {
	sf::Vector2f offset;

	font.loadFromFile("resources/fonts/youngserif-regular.otf");

	output.setString(message);
	output.setFont(font);
	output.setCharacterSize(35);
	output.setFillColor(sf::Color::White);

	offset = sf::Vector2f(output.getLocalBounds().width / 2, output.getLocalBounds().height / 2);

	output.setPosition(sf::Vector2f(SharedData::referenceWindow->getSize() / (unsigned int) 2) - offset);
};

void MessageState::atFinish() {

};

void MessageState::update(sf::Time dt) {
	accumulated += dt;

	SharedData::renderTo->draw(output);
	if (accumulated.asSeconds() >= seconds) {
		SharedData::stateManager->popState();
	}
};