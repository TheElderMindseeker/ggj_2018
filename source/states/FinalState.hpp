//
// Created by Alex on 28.01.2018.
//

#ifndef GGJ_2018_FINALSTATE_HPP
#define GGJ_2018_FINALSTATE_HPP

#include "Shared.hpp"

class FinalState : public GameState {
public:
	void atStart() override {};

	void atFinish() override {};

	void update(sf::Time dt) override { SharedData::referenceWindow->close(); };
};

#endif //GGJ_2018_FINALSTATE_HPP
