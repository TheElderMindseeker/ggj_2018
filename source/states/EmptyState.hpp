#ifndef EFTNC_EMPTYSTATE_HPP
#define EFTNC_EMPTYSTATE_HPP

#include "GameState.hpp"

class EmptyState : public GameState {
public:
	EmptyState() = default;

	void atStart();

	void atFinish();

	void update(sf::Time dt);
};

#endif // EFTNC_EMPTYSTATE_HPP
