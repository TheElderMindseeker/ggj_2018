#ifndef EFTNC_GAME_STATE_HPP
#define EFTNC_GAME_STATE_HPP

#include <SFML/Graphics.hpp>

/**
 * Class representing the state of the game: main menu, gameplay itself or pause menu.
 * This is abstract class, it will have descendants who will implement the functions
 * Each descendant will have unique ID for using in FSM. update function is returning
 * exactly the ID of the desired state.
 */

class GameState {
public:
	/**
	 *  This function should be executed by FSM when state is pushed
	 */
	virtual void atStart() = 0;

	/**
	 *  This function should be executed by FSM when state is popped
	 */
	virtual void atFinish() = 0;


	/**
	 * Handle events, update objects
	 * @param dt - time elapsed
	 */
	virtual void update(sf::Time dt) = 0;
};


#endif //EFTNC_GAME_STATE_HPP
