#include "EmptyState.hpp"
#include <Shared.hpp>

void EmptyState::atStart() {
	// Nothing to do
}

void EmptyState::atFinish() {
	// Nothing to do
}

void EmptyState::update(sf::Time dt) {
	SharedData::systemManager->updateAll(dt);
}
