#ifndef EFTNC_STATE_MANAGER_HPP
#define EFTNC_STATE_MANAGER_HPP

#include "states/GameState.hpp"

#include <stack>
#include <vector>
#include <memory>

/**
 * This class encapsulates the logic of state manager.
 * It is used for controlling the game states during the execution
 * of the program. The class is based upon the vector data structure,
 * however, it acts as stack itself. This decision is based on ease of
 * iterating for drawing purposes.
 */

class StateManager {
public:
	StateManager();
	/**
	 * These functions act on the stack. 'push' adds non-void state to the stack.
	 * 'pop' removes the top element from the stack if any is present and returns it
	 * to the caller. 'change' replaces the state on the top of the stack and returns
	 * the removed element.
	 * Be careful with void-pointers, functions do not add void-pointers to the stack
	 * and return void-pointers if the stack is empty but you try to access any elements.
	 * */

	/**
	 * Push smart pointer to the new state to the states stack
	 * @param s - state to push
	 */
	void pushState(std::unique_ptr<GameState> s);

	/**
	 * Replace top state with new one
	 * @param s - state to replace top state with
	 */
	void changeState(std::unique_ptr<GameState> s);

	/**
	 * Remove top state, i.e. unpause game
	 */
	void popState(); // Remove the state from the top

	void update(sf::Time dt);

private:
	std::vector<std::unique_ptr<GameState>> states;

};


#endif //EFTNC_STATE_MANAGER_HPP
