#ifndef GGJ_REEF_STATE_HPP
#define GGJ_REEF_STATE_HPP

#include "GameState.hpp"
#include "systems/InteractionSystem.hpp"

class ReefState : public GameState {
public:
	ReefState(int level_number) : levelNumber(level_number) {};

	void atStart() override;

	void atFinish() override;

	void update(sf::Time dt) override;


private:
	InteractionSystem *interactiveSystem;

	int levelNumber;

	bool message_shown = false;
};

#endif // GGJ_REEF_STATE_HPP
