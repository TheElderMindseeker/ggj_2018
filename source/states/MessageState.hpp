//
// Created by Alex on 27.01.2018.
//

#ifndef GGJ_2018_MESSAGESTATE_HPP
#define GGJ_2018_MESSAGESTATE_HPP

#include "GameState.hpp"

class MessageState : public GameState {
public:
	MessageState(uint8_t seconds, std::string message) : seconds(seconds), message(message),
														 accumulated(sf::Time::Zero) {};

	void atStart() override;

	void atFinish() override;

	void update(sf::Time dt) override;

private:
	uint8_t seconds;
	std::string message;

	sf::Text output;
	sf::Font font;

	sf::Time accumulated;
};


#endif //GGJ_2018_MESSAGESTATE_HPP
