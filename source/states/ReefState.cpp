#include "ReefState.hpp"
#include <systems/PositionSystem.hpp>
#include <systems/ControlsSystem.hpp>
#include <systems/GraphicsSystem.hpp>
#include <MapLoader.hpp>
#include "systems/RadarSystem.hpp"
#include "states/MessageState.hpp"
#include "states/FinalState.hpp"

void ReefState::atStart() {

	if (levelNumber > 0)
		dynamic_cast<RadarSystem *>(SharedData::systemManager->getSystem(RadarSystem::getID()))->createBeam();
	else
		levelNumber = 1;


	SharedData::entityManager->createFromSchema("background.json");

	interactiveSystem = dynamic_cast<InteractionSystem *>(SharedData::systemManager->getSystem(
			InteractionSystem::getID()));

	MapLoader::load_map("map" + std::to_string(levelNumber) + ".json");

	SharedData::stateManager
			->pushState(std::make_unique<MessageState>(3,
													   "Get "
													   + std::to_string(interactiveSystem->getFriendsNeeded())
													   + " allies to your location."
						)
			);

	SharedData::stateManager
			->pushState(std::make_unique<MessageState>(3,
													   "You need to destroy "
													   + std::to_string(interactiveSystem->getEnemiesNeeded())
													   + " enemies."
						)
			);
}

void ReefState::atFinish() {
	interactiveSystem->clearWinLoseConditions();
	interactiveSystem->setEnemiesSunk(0);
	interactiveSystem->setFriendsSunk(0);
	interactiveSystem->setFriendsHarboured(0);
	SharedData::entityManager->clear();
}

void ReefState::update(sf::Time dt) {
	if (interactiveSystem->isGameWon()) {
		if (message_shown)
			SharedData::stateManager->changeState(std::make_unique<ReefState>(levelNumber + 1));
		else {
			if (!MapLoader::map_exists("map" + std::to_string(levelNumber + 1) + ".json")) {
				SharedData::stateManager->popState();
				SharedData::stateManager->pushState(std::make_unique<FinalState>());
				std::string gj = "Great job on helping your allies\n"
						"and misleading our foes.\n"
						"You truly are an evil mastermind!";
				SharedData::stateManager->pushState(
						std::make_unique<MessageState>(7, gj));
			} else
				SharedData::stateManager->pushState(std::make_unique<MessageState>(5, "Get ready for the next level"));
			message_shown = true;
		}
	} else if (interactiveSystem->isGameLost()) {
		if (message_shown)
			SharedData::stateManager->changeState(std::make_unique<ReefState>(levelNumber));
		else {
			SharedData::stateManager->pushState(std::make_unique<MessageState>(3, "Try again"));
			message_shown = true;
		}
	} else {
		SharedData::systemManager->
				updateAll(dt);
	}
}
