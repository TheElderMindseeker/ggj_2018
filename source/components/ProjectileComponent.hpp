#ifndef EFTNC_PROJECTILECOMPONENT_HPP
#define EFTNC_PROJECTILECOMPONENT_HPP

struct ProjectileComponent {
	// The ship that launched a projetile
	eftnc::entity_t mother_ship;
};

#endif // EFTNC_PROJECTILECOMPONENT_HPP
