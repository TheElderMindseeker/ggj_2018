#ifndef EFTNC_CONTROLS_COMPONENT_HPP
#define EFTNC_CONTROLS_COMPONENT_HPP

#include "Types.hpp"
#include "components/inputs/input.hpp"

#include <map>
#include <string>
#include <memory>

struct ControlsComponent {
	eftnc::entity_t entity_id;
	std::map<eftnc::input::Action, std::shared_ptr<Input>> controls;
};


#endif //EFTNC_CONTROLS_COMPONENT_HPP
