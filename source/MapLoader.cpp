//
// Created by Alex on 27.01.2018.
//

#include "MapLoader.hpp"
#include "systems/ShipSystem.hpp"
#include "Shared.hpp"
#include "systems/InteractionSystem.hpp"

#include <fstream>

void MapLoader::load_map(const std::string &filename) {
	std::ifstream map("resources/maps/" + filename);
	nlohmann::json json;

	if (!map.good())
		throw new eftnc::generic_exception("MapLoader: Failed to open the map");

	map >> json;

	eftnc::entity_t entity;

	// Cached outside of loop
	std::string key;
	nlohmann::json values;

	SharedData::entityManager->createFromSchema("island.json");

	for (nlohmann::json objects:json) {
		// Is the given part is a schema part or a control part?
		if (objects.find("schema") != objects.end()) {
			std::string schema = objects["schema"].get<std::string>();
			objects.erase("schema");
			entity = SharedData::entityManager->createFromSchema(schema);
			for (nlohmann::json::iterator params = objects.begin(); params != objects.end(); ++params) {
				key = params.key();
				values = params.value();
				SharedData::systemManager->getSystem(key)->assemble(entity, values);
			}
		} else {
			std::string number;
			int color = 0;
			ShipOrder controls[4];
			ShipSystem *ship_sys = dynamic_cast<ShipSystem *>(SharedData::systemManager->getSystem(
					ShipSystem::getID()));
			InteractionSystem *interaction_system = dynamic_cast<InteractionSystem *>(
					SharedData::systemManager->getSystem(InteractionSystem::getID()));
			int friendly_needed = objects.value("friendly_needed", 0);
			int enemy_needed = objects.value("enemy_needed", 0);
			int friendly_total = objects.value("friendly_total", 0);
			int enemy_total = objects.value("enemy_total", 0);

			interaction_system->setEnemiesNeeded(enemy_needed);
			interaction_system->setFriendsNeeded(friendly_needed);
			interaction_system->setEnemiesTotal(enemy_total);
			interaction_system->setFriendsTotal(friendly_total);

			objects.erase("friendly_needed");
			objects.erase("enemy_needed");
			objects.erase("friendly_total");
			objects.erase("enemy_total");

			for (nlohmann::json::iterator params = objects.begin(); params != objects.end(); ++params) {
				number = params.key();
				color = std::atoi(number.c_str());
				for (int c = 0; c < 4; ++c) {
					controls[c] = params.value()[c];
				}
				ship_sys->setControl(color, controls);
			}
		}
	}

	map.close();
}

bool MapLoader::map_exists(const std::string &filename) {
	std::ifstream test("resources/maps/" + filename);
	return test.good();
}